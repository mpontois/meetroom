import { Equipement } from "./Equipement";

export interface Room {
  name: string;
  description: string;
  capacity: number;
  equipements: Equipement[];
  createdAt: Date;
  updatedAt: Date;
}
